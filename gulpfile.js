var gulp = require('gulp');
var sass = require('gulp-sass');
var plumber = require('gulp-plumber');
var browserSync = require('browser-sync');
var fileinclude = require('gulp-file-include');
var sourcemaps = require('gulp-sourcemaps');

var paths = {
	sass: './src/scss/**/*.scss',
	//scss: './src/scss/**/*.scss',
	html: './src/rawHtml/*.html',
	templateHtml: './src/template/*.html',
	js: './src/js/*.js'
}

gulp.task('sass', function () {
	 gulp.src(paths.sass)
	 	.pipe(sourcemaps.init())
	 	 .pipe(plumber())
		 // .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
		 .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
		 .pipe(sourcemaps.write())
		 .pipe(gulp.dest('./dist/css/agenda'))
		 .pipe(browserSync.reload({stream: true}));
});

gulp.task('js', function () {
	 gulp.src(paths.js)
		 .pipe(plumber())
		 // .pipe(js())
		 .pipe(gulp.dest('./dist/js/'))
		 .pipe(browserSync.reload({stream: true}));
});

gulp.task('fileinclude', function() {
  gulp.src([paths.html])
    .pipe(fileinclude({
      prefix: '@@',
      basepath: '@file'
    }))
    .pipe(gulp.dest('./dist/'));
});


//Static server
gulp.task('serve', ['fileinclude', 'js'], function() {
	browserSync({
		port: 61613,
		server: {
			baseDir: './dist'
		}
	});
	
	gulp.watch(paths.html).on('change', function(option){
		// console.log(option.path);
		gulp.src([option.path])
	    .pipe(fileinclude({
	      prefix: '@@',
	      basepath: '@file'
	    }))
	    .pipe(gulp.dest('./dist/'));
	    browserSync.reload();
	});


	gulp.watch(paths.templateHtml, [fileinclude]).on('change', browserSync.reload);
	gulp.watch(paths.sass, ['sass']);
	gulp.watch(paths.js, ['js']);
});



gulp.task('build', function() {
	gulp.src('./css/*.css')//header&footer 的style
		.pipe(gulp.dest('./dist/css'));
	gulp.src('*.html')
		.pipe(gulp.dest('./dist/'));
	gulp.src('./images/**/*')
	    .pipe(gulp.dest('./dist/images/'));
	// gulp.src('./css/*.css')
	//     .pipe(gulp.dest('./dist/css/'));
	gulp.src('./CommonPage/*')
	    .pipe(gulp.dest('./dist/CommonPage/'));
	gulp.src('./js/*.js')
	    .pipe(gulp.dest('./dist/js/'));
});



//start build server
gulp.task('serve:dist', ['build'], function() {
	browserSync.init({
		server: {
			baseDir: './dist/'
		}
	});
});

gulp.task('default', ['serve'], function(){
	
});

